/*
 * Copyright 2018 Dan Malec
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

class RequestBuilder {

  constructor() {
  }

  withAccessToken(accessToken) {
    this._accessToken = accessToken;
    return this;
  }

  withApplicationId(applicationId) {
    this._applicationId = applicationId;
    return this;
  }

  withIntent(intent) {
    this._intent = intent;
    return this;
  }

  withLocale(locale) {
    this._locale = locale;
    return this;
  }

  withNewConversationFlag(newConversationFlag) {
    this._newConversationFlag = newConversationFlag;
    return this;
  }

  withRequestId(requestId) {
    this._requestId = requestId;
    return this;
  }

  withSessionAttributes(sessionAttributes) {
    this._sessionAttributes = sessionAttributes;
    return this;
  }

  withSessionId(sessionId) {
    this._sessionId = sessionId;
    return this;
  }

  withSlot(slots) {
    this._slots = slots;
    return this;
  }

  withUserId(userId) {
    this._userId = userId;
    return this;
  }

  /*
   * The following method is copied/modified from alexa-conversation:
   * https://github.com/ExpediaDotCom/alexa-conversation
   *
   * Licensed under the MIT License.
   *
   * Copyright (c) 2017 Joan Gamell Farre, Expedia Inc.
   *
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is
   * furnished to do so, subject to the following conditions:
   *
   * The above copyright notice and this permission notice shall be included in all
   * copies or substantial portions of the Software.
   */
  build() {
    return {
      session: {
        sessionId: this._sessionId,
        application: {
          applicationId: this._applicationId,
        },
        attributes: this._sessionAttributes,
        user: {
          userId: this._userId,
          accessToken: this._accessToken,
        },
        new: this._newConversationFlag,
      },
      request: {
        type: 'IntentRequest',
        requestId: this._requestId,
        locale: this._locale,
        timestamp: (new Date()).toISOString(),
        intent: {
          name: this._intent,
          slots: this._slots,
        },
      },
      version: '1.0',
    };
  };
}

module.exports = { RequestBuilder };
