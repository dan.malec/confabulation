/*
 * Copyright 2018 Dan Malec
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const aws = require('aws-sdk');

class DbWriteParamsBuilder {

  withData(data) {
    this._data = data;
    return this;
  }

  withTableName(tableName) {
    this._tableName = tableName;
    return this;
  }

  withUserId(userId) {
    this._userId = userId;
    return this;
  }

  build() {
    return {
      Item: {
        userId: this._userId,
        mapAttr: this._data,
      },
      TableName: this._tableName,
    };
  }
}

class DbTableDefinitionBuilder {

  withTableName(tableName) {
    this._tableName = tableName;
    return this;
  }

  build() {
    return {
      TableName: this._tableName,
      KeySchema: [{
        AttributeName: 'userId',
        KeyType: 'HASH',
      }],
      AttributeDefinitions: [{
        AttributeName: 'userId',
        AttributeType: 'S',
      }],
      ProvisionedThroughput: {
        ReadCapacityUnits: 5,
        WriteCapacityUnits: 5,
      },
    };
  }
}

class DbEndpoint {
  withDbUrl(dbUrl) {
    this._dbUrl = dbUrl;
    return this;
  }

  withUserId(userId) {
    this._userId = userId;
    return this;
  }

  withTableName(tableName) {
    this._tableName = tableName;
    return this;
  }

  connect() {
    this._humbleCreateDbConnection();
    return this;
  }

  writeData(data, callback) {
    const tableDefinition = new DbTableDefinitionBuilder()
      .withTableName(this._tableName)
      .build();

    const writeParams = new DbWriteParamsBuilder()
      .withTableName(this._tableName)
      .withUserId(this._userId)
      .withData(data)
      .build();

    const self = this;
    self._humbleCreateTable(tableDefinition)
      .then(() => {
        self._humbleWaitForTableToExist(self._tableName);
        self._humbleStoreData(writeParams, callback);
      }).catch((error) => {
        if (error.code !== 'ResourceInUseException' || error.message !== 'Cannot create preexisting table') {
          console.log(error);
        }
        self._humbleStoreData(writeParams, callback);
      });
  }

  // ------------------------------------
  // Humble function to ease unit testing
  // ------------------------------------

  /* istanbul ignore next */
  _humbleCreateDbConnection() {
    this._db = new aws.DynamoDB({
      endpoint: this._dbUrl,
      accessKeyId: 'dynamo',
      region: 'test',
      secretAccessKey: 'secret',
    });

    this._docDb = new aws.DynamoDB.DocumentClient({
      service: this._db,
    });
  }

  /* istanbul ignore next */
  _humbleCreateTable(tableDefinition) {
    return this._db.createTable(tableDefinition).promise();
  }

  /* istanbul ignore next */
  _humbleWaitForTableToExist(tableName) {
    this._db.waitFor('tableExists', { TableName: tableName});
  }

  /* istanbul ignore next */
  _humbleStoreData(params, callback) {
    // TODO: just pass callback?
    this._docDb.put(params, callback);
  }
}

module.exports = { DbEndpoint, DbWriteParamsBuilder, DbTableDefinitionBuilder };
