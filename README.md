# Confabulation

[![pipeline status](https://gitlab.com/dan.malec/confabulation/badges/master/pipeline.svg)](https://gitlab.com/dan.malec/confabulation/commits/master)
[![coverage report](https://gitlab.com/dan.malec/confabulation/badges/master/coverage.svg)](https://gitlab.com/dan.malec/confabulation/commits/master)

Confabulation is a JavaScript test tool to assist with BDD / Cucumber for Amazon Alexa skills.

## Help & support

* Issue Tracker: https://gitlab.com/dan.malec/confabulation/issues

## Install

Confabulation is available as an npm module.

``` shell
$ npm install confabulation
```

## Dependencies

Confabulation depends on a local DynamoDB instance being present.  It has been tested with both
* [DynamoDB Local JAR](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html)
* [DynamoDB Local Docker](https://hub.docker.com/r/dwmkerr/dynamodb/)

This requires a modification to your skill's index.js.

``` javascript
const aws = require('aws-sdk');

// ...

exports.handler = function(event, context, callback) {
  const alexa = Alexa.handler(event, context);

  if (process.env.NODE_ENV === 'test') {
    alexa.dynamoDBClient = new aws.DynamoDB({
      endpoint: process.env.ENDPOINT_URL || 'http://localhost:8000/',
      accessKeyId: 'dynamo',
      region: 'test',
      secretAccessKey: 'secret',
    });
  }

  // ...
};
```

And a change to your package.json:
``` json
  "scripts": {
    "cucumber": "NODE_ENV=test cucumber-js"
  }
```

And changes to CI (E.G. .gitlab-ci.yml):
``` yaml
services:
  - name: dwmkerr/dynamodb
    alias: dynamodb

variables:
  ENDPOINT_URL: "http://dynamodb:8000/"
```

## Usage

### Setup

Create an endpoint in world.js:
``` javascript
const skill = require('../../index.js');
const { setWorldConstructor } = require('cucumber');
const { Endpoint } = require('confabulation');

class CustomWorld {
  constructor({attach, parameters}) {
    this.attach = attach;
    this.parameters = parameters;
    this.endpoint = new Endpoint()
      .withSkill(skill)
      .withApplicationId('<your-skill-id>')
      .withTableName('<your-table-name>')
      .withDbUrl(process.env.ENDPOINT_URL || 'http://localhost:8000/')
  }
}

setWorldConstructor(CustomWorld);
```

### Invoking the skill

You can then add invocations in steps:
``` javascript
When('the user opens the skill', function(callback) {
  this.endpoint.say(new Intent('LaunchIntent'), callback);
}
```

Including passing in slot data:
``` javascript
When('the user says the number {int}', function(number, callback) {
  this.endpoint.say(new Intent('CountIntent').withSlot('count', number), callback);
});

When('the user says {word}', function(value, callback) {
  const value_id = value.toUpperCase();
  this.endpoint.say(new Intent('SelectListItemIntent').withSlot('list_item', value, value_id), callback);
});
```

### Working with session data

You can set session data:
``` javascript
Given('the user has {int} items', function(value) {
  this.endpoint.setAttribute('item_count', value);
});
```

And check session data:
``` javascript
Then('the player should have {int} items', function(value) {
  expect(this.endpoint.getAttribute('item_count'))).to.equal(value);
});
```

### Working with response data

You can check the response:
``` javascript
Then('the user should hear {string}', function(phrase) {
  expect(this.endpoint.response.response.outputSpeech.ssml).to.contain(phrase);
});
```

## Credits

Portions of confabulation are copied/modified from Joan Gamell Farre's alexa-conversation module (as indicated in the source).
* [alexa-conversation](https://github.com/ExpediaDotCom/alexa-conversation)
