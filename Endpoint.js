/*
 * Copyright 2018 Dan Malec
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const { DbEndpoint } = require('./DbUtils');
const { RequestBuilder } = require('./RequestBuilder');

class Endpoint {
  /**
   * The following method is copied/modified from alexa-conversation:
   * https://github.com/ExpediaDotCom/alexa-conversation
   *
   * Licensed under the MIT License.
   *
   * Copyright (c) 2017 Joan Gamell Farre, Expedia Inc.
   *
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is
   * furnished to do so, subject to the following conditions:
   *
   * The above copyright notice and this permission notice shall be included in all
   * copies or substantial portions of the Software.
   */
  constructor() {
    this._sessionId = 'SessionId.ee2e2123-75dc-4b32-bf87-8633ba72c294';

    this._userId = 'amzn1.ask.account.AHEYQEFEHVSPRHPZS4ZKSLDADKC62MMFTEC7MVZ636U56XIFWC'
      + 'FUAJ2Q2RJE47PNDHDBEEMMDTEQXWFSK3OPALF4G2D2QAJW4SDMEI5DCULK5G4R32T76G5SZIWDMJ2ZZ'
      + 'Q37UYH2BIXBQ3GIGEBIRW4M4YV5QOQG3JXHB73CTH6AAPYZBOIQE5N3IKUETT54HMTRUX2EILTFGWQ';

    this._accessToken = '0b42d14150e71fb356f2abc42f5bc261dd18573a86a84aa5d7a74592b505a0b7';

    this._requestId = 'EdwRequestId.33ac9138-640f-4e6e-ab71-b9619b2c2210';

    this._locale = 'en-US';

    this._firstInvocation = true;
    this._sessionAttributes = {};
  }

  withAccessToken(accessToken) {
    this._accessToken = accessToken;
    return this;
  }

  withApplicationId(applicationId) {
    this._applicationId = applicationId;
    return this;
  }

  withDbUrl(dbUrl) {
    this._dbUrl = dbUrl;
    return this;
  }

  withLocale(locale) {
    this._locale = locale;
    return this;
  }

  withRequestId(requestId) {
    this._requestId = requestId;
    return this;
  }

  withResponseListener(responseListener) {
    this._responseListener = responseListener;
    return this;
  }

  withSessionId(sessionId) {
    this._sessionId = sessionId;
    return this;
  }

  withSkill(skill) {
    this._skill = skill;
    return this;
  }

  withTableName(tableName) {
    this._tableName = tableName;
    return this;
  }

  withUserId(userId) {
    this._userId = userId;
    return this;
  }

  get response() {
    return this._response;
  }

  say(intent, callback) {
    const self = this;

    const request = new RequestBuilder()
      // Static Configuration
      .withAccessToken(this._accessToken)
      .withApplicationId(this._applicationId)
      .withLocale(this._locale)
      .withRequestId(this._requestId)
      .withSessionId(this._sessionId)
      .withUserId(this._userId)
      // Session Information
      .withNewConversationFlag(this._firstInvocation)
      .withSessionAttributes(this._sessionAttributes)
      // Request Information
      .withIntent(intent.name)
      .withSlot(intent.buildSlots())
      .build();

    if (this._firstInvocation) {
      this._firstInvocation = false;

      const dbEndpoint = new DbEndpoint()
        .withDbUrl(this._dbUrl)
        .withTableName(this._tableName)
        .withUserId(this._userId)
        .connect();

      dbEndpoint.writeData(this._sessionAttributes, function() {
        self._skill.handler(request, {
          succeed: function(response) {
            self._response = response;
            self._sessionAttributes = response.sessionAttributes;
            self._callResponseListener();
            callback.call(self);
          },
          fail: function(response) {
            self._response = response;
            self._sessionAttributes = response.sessionAttributes;
            self._callResponseListener();
            // TODO: SOMETHING FOR ERROR
            callback.call(self);
          },
        });
      });
    } else {
      this._skill.handler(request, {
        succeed: function(response) {
          self._response = response;
          self._sessionAttributes = response.sessionAttributes;
          self._callResponseListener();
          callback.call(self);
        },
        fail: function(response) {
          self._response = response;
          self._sessionAttributes = response.sessionAttributes;
          self._callResponseListener();
          // TODO: SOMETHING FOR ERROR
          callback.call(self);
        },
      });
    }
  }

  setAttribute(name, value) {
    this._sessionAttributes[name] = value;
  }

  setAttributes(map) {
    for (const key of Object.keys(map)) {
      this._sessionAttributes[key] = map[key];
    }
  }

  getAttribute(name) {
    return this._sessionAttributes[name];
  }

  clearAttribute(name) {
    delete this._sessionAttributes[name];
  }

  _callResponseListener() {
    if (this._responseListener) {
      this._responseListener(this._response, this._sessionAttributes);
    }
  }
}

module.exports = { Endpoint };
