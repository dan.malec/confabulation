/*
 * Copyright 2018 Dan Malec
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);

const { DbEndpoint, DbWriteParamsBuilder, DbTableDefinitionBuilder } = require('../DbUtils');

describe('DbEndpoint.writeData()', function() {
  let humbleCreateTableStub;
  let humbleWaitForTableToExistStub;
  let humbleStoreDataStub;

  before(function() {
    humbleCreateTableStub = sinon.stub(DbEndpoint.prototype, '_humbleCreateTable');
    humbleWaitForTableToExistStub = sinon.stub(DbEndpoint.prototype, '_humbleWaitForTableToExist');
    humbleStoreDataStub = sinon.stub(DbEndpoint.prototype, '_humbleStoreData');
  });

  beforeEach(function() {
    humbleCreateTableStub.reset();
    humbleWaitForTableToExistStub.reset();
    humbleStoreDataStub.reset();
  });

  after(function() {
    humbleCreateTableStub.restore();
    humbleWaitForTableToExistStub.restore();
    humbleStoreDataStub.restore();
  });

  it('should handle a brand new database', function(done) {
    const cut = new DbEndpoint()
      .withDbUrl('TestDbUrl')
      .withUserId('TestUserId')
      .withTableName('TestTableName');
    const testData = {TestKey: 'TestValue'};
    const expectedData = new DbWriteParamsBuilder()
      .withUserId('TestUserId')
      .withTableName('TestTableName')
      .withData(testData)
      .build();
    humbleCreateTableStub.resolves();
    humbleStoreDataStub.yields();

    cut.writeData(testData, function() {
      expect(humbleWaitForTableToExistStub).to.have.been.called;
      expect(humbleStoreDataStub).to.have.been.calledWith(expectedData);
      done();
    });
  });

  it('should handle an existing database', function(done) {
    const cut = new DbEndpoint()
      .withDbUrl('TestDbUrl')
      .withUserId('TestUserId')
      .withTableName('TestTableName');
    const testData = {TestKey: 'TestValue'};
    const expectedData = new DbWriteParamsBuilder()
      .withUserId('TestUserId')
      .withTableName('TestTableName')
      .withData(testData)
      .build();
    humbleCreateTableStub.rejects(Error('OH NO!'));
    humbleStoreDataStub.yields();

    cut.writeData(testData, function() {
      expect(humbleWaitForTableToExistStub).to.have.not.been.called;
      expect(humbleStoreDataStub).to.have.been.calledWith(expectedData);
      done();
    });
  });
});

describe('DbWriteParamsBuilder.build()', function() {
  it('should return the passed in parameters', function() {
    const cut = new DbWriteParamsBuilder()
      .withUserId('TestUserId')
      .withTableName('TestTableName')
      .withData({ TestAttrKey: 'TestAttrValue'});

    const json = cut.build();

    expect(json.Item.userId).to.equal('TestUserId');
    expect(json.Item.mapAttr).to.have.own.property('TestAttrKey');
    expect(json.TableName).to.equal('TestTableName');
  });
});

describe('DbTableDefinitionBuilder.build()', function() {
  it('should return the passed in parameters', function() {
    const cut = new DbTableDefinitionBuilder()
      .withTableName('TestTableName');

    const json = cut.build();

    expect(json.TableName).to.equal('TestTableName');
    expect(json.KeySchema[0].AttributeName).to.equal('userId');
    expect(json.KeySchema[0].KeyType).to.equal('HASH');
    expect(json.AttributeDefinitions[0].AttributeName).to.equal('userId');
    expect(json.AttributeDefinitions[0].AttributeType).to.equal('S');
  });
});
