/*
 * Copyright 2018 Dan Malec
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const expect = require('chai').expect;
const { Intent, Slot } = require('../Intent');

describe('Intent.build()', function() {
  it('should handle an intent with no slots', function() {
    const cut = new Intent('MyIntent');

    const json = cut.buildSlots();

    expect(json).to.be.empty;
  });

  it('should handle an intent with one slot', function() {
    const cut = new Intent('MyIntent').withSlot('SlotName', 42);

    const json = cut.buildSlots();

    expect(json).to.have.own.property('SlotName');
  });

  it('should handle an intent with two slots', function() {
    const cut = new Intent('MyIntent').withSlot('NumberSlot', 42).withSlot('EnumSlot', 'spoken', 'id');

    const json = cut.buildSlots();

    expect(json).to.have.own.property('NumberSlot');
    expect(json).to.have.own.property('EnumSlot');
  });
});

describe('Intent.name', function() {
  it('should return the passed in intent name', function() {
    const cut = new Intent('IntentName');

    const name = cut.name;

    expect(name).to.equal('IntentName');
  });
});

describe('Slot.build()', function() {
  it('should handle a number', function() {
    const cut = new Slot('number', 42);

    const json = cut.build();

    expect(json.name).to.equal('number');
    expect(json.value).to.equal(42);
  });

  it('should handle an enum', function() {
    const cut = new Slot('enum', 'Spoken Value', 'VALUE_ID');

    const json = cut.build();

    expect(json.name).to.equal('enum');
    expect(json.value).to.equal('Spoken Value');
    expect(json.resolutions.resolutionsPerAuthority[0].values[0].value.id).to.equal('VALUE_ID');
  });
});

describe('Slot.name', function() {
  it('should return the passed in slot name', function() {
    const cut = new Slot('SlotName', 'value');

    const name = cut.name;

    expect(name).to.equal('SlotName');
  });
});
