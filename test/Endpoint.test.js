/*
 * Copyright 2018 Dan Malec
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);

const { Endpoint } = require('../Endpoint');
const { DbEndpoint } = require('../DbUtils');
const { Intent } = require('../Intent');

describe('Endpoint setters', function() {
  it('should store the passed in parameters', function() {
    const cut = new Endpoint()
      .withAccessToken('TestAccessToken')
      .withApplicationId('TestApplicationId')
      .withDbUrl('TestDbUrl')
      .withLocale('TestLocale')
      .withRequestId('TestRequestId')
      .withSessionId('TestSessionId')
      .withTableName('TestTableName')
      .withUserId('TestUserId');

    expect(cut._accessToken).to.equal('TestAccessToken');
    expect(cut._applicationId).to.equal('TestApplicationId');
    expect(cut._dbUrl).to.equal('TestDbUrl');
    expect(cut._locale).to.equal('TestLocale');
    expect(cut._requestId).to.equal('TestRequestId');
    expect(cut._sessionId).to.equal('TestSessionId');
    expect(cut._tableName).to.equal('TestTableName');
    expect(cut._userId).to.equal('TestUserId');
  });
});

describe('Endpoint attributes', function() {

  it('should set/get individuals', function() {
    const cut = new Endpoint();

    cut.setAttribute('TestKey', 'TestValue');

    expect(cut.getAttribute('TestKey')).to.equal('TestValue');
  });

  it('should set multiples', function() {
    const cut = new Endpoint();

    cut.setAttributes({
      TestKey1: 'TestValue1',
      TestKey2: 'TestValue2',
    });

    expect(cut.getAttribute('TestKey1')).to.equal('TestValue1');
    expect(cut.getAttribute('TestKey2')).to.equal('TestValue2');
  });

  it('should clear individuals', function() {
    const cut = new Endpoint();
    cut.setAttribute('TestKey', 'TestValue');

    cut.clearAttribute('TestKey');

    expect(cut.getAttribute('TestKey')).to.be.undefined;
  });
});


describe('Endpoint.say()', function() {
  let humbleCreateDbConnectionStub;
  let writeDataStub;
  let handlerStub;

  before(function() {
    humbleCreateDbConnectionStub = sinon.stub(DbEndpoint.prototype, '_humbleCreateDbConnection');
    writeDataStub = sinon.stub(DbEndpoint.prototype, 'writeData');
    handlerStub = sinon.stub();
  });

  beforeEach(function() {
    writeDataStub.reset();
    handlerStub.reset();
  });

  after(function() {
    humbleCreateDbConnectionStub.restore();
    writeDataStub.restore();
  });

  it('should handle the first invocation', function(done) {
    const cut = new Endpoint()
      .withSkill({ handler: handlerStub});
    writeDataStub.yields();
    handlerStub.yieldsTo('succeed', {
      sessionAttributes: {},
    });

    cut.say(new Intent('MyIntent'), function() {
      expect(writeDataStub).to.have.been.calledWith({});
      expect(handlerStub).to.have.been.called;
      done();
    });
  });

  it('should handle the second invocation', function(done) {
    const cut = new Endpoint()
      .withSkill({ handler: handlerStub});
    cut._firstInvocation = false;
    handlerStub.yieldsTo('succeed', {
      sessionAttributes: {},
    });

    cut.say(new Intent('MyIntent'), function() {
      expect(writeDataStub).to.have.not.been.called;
      expect(handlerStub).to.have.been.called;
      done();
    });
  });
});
