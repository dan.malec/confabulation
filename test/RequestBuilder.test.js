/*
 * Copyright 2018 Dan Malec
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const expect = require('chai').expect;
const { RequestBuilder } = require('../RequestBuilder');

describe('RequestBuilder.build()', function() {
  it('should return the passed in parameters', function() {
    const cut = new RequestBuilder()
      // Session Attributes
      .withAccessToken('TestAccessToken')
      .withApplicationId('TestApplicationId')
      .withNewConversationFlag(true)
      .withSessionId('TestSessionId')
      .withSessionAttributes({ TestAttrKey: 'TestAttrValue'})
      .withUserId('TestUserId')
      // Request Attributes
      .withIntent('TestIntentName')
      .withLocale('TestLocale')
      .withRequestId('TestRequestId')
      .withSlot({});

    const json = cut.build();

    // Session Attributes
    expect(json.session.user.accessToken).to.equal('TestAccessToken');
    expect(json.session.application.applicationId).to.equal('TestApplicationId');
    expect(json.session.new).to.be.true;
    expect(json.session.sessionId).to.equal('TestSessionId');
    expect(json.session.attributes).to.have.own.property('TestAttrKey');
    expect(json.session.user.userId).to.equal('TestUserId');

    // Request Attributes
    expect(json.request.intent.name).to.equal('TestIntentName');
    expect(json.request.locale).to.equal('TestLocale');
    expect(json.request.requestId).to.equal('TestRequestId');
    expect(json.request.intent.slots).to.be.empty;
  });
});
