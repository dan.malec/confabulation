/*
 * Copyright 2018 Dan Malec
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

class Intent {

  constructor(intentName) {
    this._intentName = intentName;
    this._slots = [];

    return this;
  }

  get name() {
    return this._intentName;
  }

  withSlot(slotName, slotValue, optionalSlotId) {
    this._slots.push(new Slot(slotName, slotValue, optionalSlotId));

    return this;
  }

  buildSlots() {
    return this._slots.reduce((map, slot) => {
      map[slot.name] = slot.build();
      return map;
    }, {});
  }
}

class Slot {
  constructor(name, value, optionalId) {
    this._name = name;
    this._value = value;
    this._optionalId = optionalId;
  }

  get name() {
    return this._name;
  }

  build() {
    if (this._optionalId) {
      return this._buildEnumSlot();
    } else {
      return { name: this._name, value: this._value};
    }
  }

  _buildEnumSlot() {
    return {
      name: this._name,
      value: this._value,
      confirmationStatus: 'NONE',
      resolutions: {
        resolutionsPerAuthority: [
          {
            status: {
              code: 'OK',
            },
            values: [
              {
                value: {
                  id: this._optionalId,
                },
              },
            ],
          },
        ],
      },
    };
  }
}

module.exports = { Intent, Slot };
